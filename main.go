package main

import (
	"flag"
	"fmt"
	"github.com/iancoleman/strcase"
	"io/ioutil"
	"os"
	"strings"
)

const defaultOut = "bitfields_gen.go"

func main() {
	mod := flag.Bool("modified", false, "Parse an archive from stdin")
	file := flag.String("dir", ".", "Parse a dir")
	out := flag.String("out", defaultOut, "File to write the generated output to")
	stdout := flag.Bool("stdout", false, "Write generated output to stdout")
	typez := flag.String("type", "", "The types to generate bitfields from")
	constPre := flag.String("constant-prefix", "", "A string to put in front of all constant names")
	funcPre := flag.String("method-prefix", "", "A string to put in front of all flag names in methods")
	defStart := flag.String("definition-start", "Flags:", "A string to put in front of all flag names in methods")
	exportedAux := flag.Bool("aux", false, "Whether to include an exported struct with a boolean field for every bit")
	marshalers := flag.String("marshallers", "", "Package names for custom (Un)Marshallers (json/yaml)")
	notFmtOut := flag.Bool("disable-fmt", false, "Disables gofmting for the output")
	flag.String("ignore", "", "") // TODO remove
	// TODO tags
	flag.Parse()

	if len(*typez) == 0 {
		fmt.Printf("no types specified")
		os.Exit(1)
	}

	g := &Generator{
		File:  *file,
		Types: strings.Split(*typez, ","),

		DefinitionStart: *defStart,

		ConstantPrefix: *constPre,
		MethodPrefix:   *funcPre,
		ExportedAux:    *exportedAux,
		CustomTags: []CustomTagConfig{
			{"json", strcase.ToSnake},
			{"yaml", strcase.ToKebab},
		},
        FormatOut: !*notFmtOut,
	}

	if *mod {
		g.Modified = os.Stdin
	}

	if *marshalers != "" {
		g.Marshallers = strings.Split(*marshalers, ",")
	}

	err := g.Parse()
	if err != nil {
		fmt.Printf("error while parsing: %q", err)
		os.Exit(2)
	}

	err = g.Generate()
	if err != nil {
		fmt.Printf("error while generating: %q", err)
		os.Exit(3)
	}

	src, err := g.Format()
	if err != nil {
		fmt.Printf("error while formatting: %q", err)
		os.Exit(4)
	}

	if *stdout {
		fmt.Println(string(src))
	}

	if *out != "" && !(*stdout && *out == defaultOut) {
		ioutil.WriteFile(*out, src, 0644)
	}
}
