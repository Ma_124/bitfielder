// This file is automatically generated by qtc from "bf.qtpl".
// See https://github.com/valyala/quicktemplate for details.

//line bf.qtpl:1
package main

//line bf.qtpl:3
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line bf.qtpl:3
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line bf.qtpl:3
func StreamBitFieldTemplate(qw422016 *qt422016.Writer, tname string, size int) {
	//line bf.qtpl:3
	qw422016.N().S(`
// New`)
	//line bf.qtpl:4
	qw422016.E().S(tname)
	//line bf.qtpl:4
	qw422016.N().S(` creates a `)
	//line bf.qtpl:4
	qw422016.E().S(tname)
	//line bf.qtpl:4
	qw422016.N().S(` with all values being 0
func New`)
	//line bf.qtpl:5
	qw422016.E().S(tname)
	//line bf.qtpl:5
	qw422016.N().S(`() `)
	//line bf.qtpl:5
	qw422016.E().S(tname)
	//line bf.qtpl:5
	qw422016.N().S(` {
	return Bf(0)
}

// Put sets the bits specified in the bitmask to 1
func (bf *`)
	//line bf.qtpl:10
	qw422016.E().S(tname)
	//line bf.qtpl:10
	qw422016.N().S(`) Put(bitmask `)
	//line bf.qtpl:10
	qw422016.E().S(tname)
	//line bf.qtpl:10
	qw422016.N().S(`) {
	*bf = *bf | bitmask
}

// Clear sets the bits specified in the bitmask to 0
func (bf *`)
	//line bf.qtpl:15
	qw422016.E().S(tname)
	//line bf.qtpl:15
	qw422016.N().S(`) Clear(bitmask `)
	//line bf.qtpl:15
	qw422016.E().S(tname)
	//line bf.qtpl:15
	qw422016.N().S(`) {
	*bf = *bf &^ bitmask
}

// Toggle toggles the bits specified in the bitmask
func (bf *`)
	//line bf.qtpl:20
	qw422016.E().S(tname)
	//line bf.qtpl:20
	qw422016.N().S(`) Toggle(bitmask `)
	//line bf.qtpl:20
	qw422016.E().S(tname)
	//line bf.qtpl:20
	qw422016.N().S(`) {
	*bf = *bf ^ bitmask
}

// Get returns the value of the bit given in the bitmask
func (bf *`)
	//line bf.qtpl:25
	qw422016.E().S(tname)
	//line bf.qtpl:25
	qw422016.N().S(`) Get(bitmask `)
	//line bf.qtpl:25
	qw422016.E().S(tname)
	//line bf.qtpl:25
	qw422016.N().S(`) bool {
	return *bf & bitmask != 0
}

// Set sets the bits specified in the bitmask to the specified value
//
// Prefer Put()/Clear()
func (bf *`)
	//line bf.qtpl:32
	qw422016.E().S(tname)
	//line bf.qtpl:32
	qw422016.N().S(`) Set(bitmask `)
	//line bf.qtpl:32
	qw422016.E().S(tname)
	//line bf.qtpl:32
	qw422016.N().S(`, val bool) {
	if val {
		bf.Put(bitmask)
	} else {
		bf.Clear(bitmask)
	}
}

// ClearAll sets all bits to 0
func (bf *`)
	//line bf.qtpl:41
	qw422016.E().S(tname)
	//line bf.qtpl:41
	qw422016.N().S(`) ClearAll() {
	*bf = 0
}

// PutAll sets all bits to 1
func (bf *`)
	//line bf.qtpl:46
	qw422016.E().S(tname)
	//line bf.qtpl:46
	qw422016.N().S(`) PutAll() {
	*bf = (1 << `)
	//line bf.qtpl:47
	qw422016.N().D(size)
	//line bf.qtpl:47
	qw422016.N().S(`)-1
}

// ToggleAll toggles all bits
func (bf *`)
	//line bf.qtpl:51
	qw422016.E().S(tname)
	//line bf.qtpl:51
	qw422016.N().S(`) ToggleAll() {
	*bf = ^*bf
}
`)
//line bf.qtpl:54
}

//line bf.qtpl:54
func WriteBitFieldTemplate(qq422016 qtio422016.Writer, tname string, size int) {
	//line bf.qtpl:54
	qw422016 := qt422016.AcquireWriter(qq422016)
	//line bf.qtpl:54
	StreamBitFieldTemplate(qw422016, tname, size)
	//line bf.qtpl:54
	qt422016.ReleaseWriter(qw422016)
//line bf.qtpl:54
}

//line bf.qtpl:54
func BitFieldTemplate(tname string, size int) string {
	//line bf.qtpl:54
	qb422016 := qt422016.AcquireByteBuffer()
	//line bf.qtpl:54
	WriteBitFieldTemplate(qb422016, tname, size)
	//line bf.qtpl:54
	qs422016 := string(qb422016.B)
	//line bf.qtpl:54
	qt422016.ReleaseByteBuffer(qb422016)
	//line bf.qtpl:54
	return qs422016
//line bf.qtpl:54
}

//line bf.qtpl:56
func StreamFlagOperations(qw422016 *qt422016.Writer, bfName, constPrefix, methodPrefix, flagName, commentText, offset string) {
	//line bf.qtpl:56
	qw422016.N().S(`
/*`)
	//line bf.qtpl:57
	qw422016.E().S(commentText)
	//line bf.qtpl:57
	qw422016.N().S(`*/
const `)
	//line bf.qtpl:58
	qw422016.E().S(constPrefix)
	//line bf.qtpl:58
	qw422016.E().S(flagName)
	//line bf.qtpl:58
	qw422016.N().S(` `)
	//line bf.qtpl:58
	qw422016.E().S(bfName)
	//line bf.qtpl:58
	qw422016.N().S(` = 1 << `)
	//line bf.qtpl:58
	qw422016.E().S(offset)
	//line bf.qtpl:58
	qw422016.N().S(`

/*
Put`)
	//line bf.qtpl:61
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:61
	qw422016.E().S(flagName)
	//line bf.qtpl:61
	qw422016.N().S(` sets the bit at offset `)
	//line bf.qtpl:61
	qw422016.E().S(offset)
	//line bf.qtpl:61
	qw422016.N().S(` to 1:

`)
	//line bf.qtpl:63
	qw422016.E().S(commentText)
	//line bf.qtpl:63
	qw422016.N().S(`
*/
func (bf *`)
	//line bf.qtpl:65
	qw422016.E().S(bfName)
	//line bf.qtpl:65
	qw422016.N().S(`) Put`)
	//line bf.qtpl:65
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:65
	qw422016.E().S(flagName)
	//line bf.qtpl:65
	qw422016.N().S(`() {
	bf.Put(`)
	//line bf.qtpl:66
	qw422016.E().S(constPrefix)
	//line bf.qtpl:66
	qw422016.E().S(flagName)
	//line bf.qtpl:66
	qw422016.N().S(`)
}

/*
Clear`)
	//line bf.qtpl:70
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:70
	qw422016.E().S(flagName)
	//line bf.qtpl:70
	qw422016.N().S(` sets the bit at offset `)
	//line bf.qtpl:70
	qw422016.E().S(offset)
	//line bf.qtpl:70
	qw422016.N().S(` to 0:

`)
	//line bf.qtpl:72
	qw422016.E().S(commentText)
	//line bf.qtpl:72
	qw422016.N().S(`
*/
func (bf *`)
	//line bf.qtpl:74
	qw422016.E().S(bfName)
	//line bf.qtpl:74
	qw422016.N().S(`) Clear`)
	//line bf.qtpl:74
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:74
	qw422016.E().S(flagName)
	//line bf.qtpl:74
	qw422016.N().S(`() {
	bf.Clear(`)
	//line bf.qtpl:75
	qw422016.E().S(constPrefix)
	//line bf.qtpl:75
	qw422016.E().S(flagName)
	//line bf.qtpl:75
	qw422016.N().S(`)
}

/*
Toggle`)
	//line bf.qtpl:79
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:79
	qw422016.E().S(flagName)
	//line bf.qtpl:79
	qw422016.N().S(` toggles the bit at offset `)
	//line bf.qtpl:79
	qw422016.E().S(offset)
	//line bf.qtpl:79
	qw422016.N().S(`:

`)
	//line bf.qtpl:81
	qw422016.E().S(commentText)
	//line bf.qtpl:81
	qw422016.N().S(`
*/
func (bf *`)
	//line bf.qtpl:83
	qw422016.E().S(bfName)
	//line bf.qtpl:83
	qw422016.N().S(`) Toggle`)
	//line bf.qtpl:83
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:83
	qw422016.E().S(flagName)
	//line bf.qtpl:83
	qw422016.N().S(`() {
	bf.Toggle(`)
	//line bf.qtpl:84
	qw422016.E().S(constPrefix)
	//line bf.qtpl:84
	qw422016.E().S(flagName)
	//line bf.qtpl:84
	qw422016.N().S(`)
}

/*
`)
	//line bf.qtpl:88
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:88
	qw422016.E().S(flagName)
	//line bf.qtpl:88
	qw422016.N().S(` returns the bit at offset `)
	//line bf.qtpl:88
	qw422016.E().S(offset)
	//line bf.qtpl:88
	qw422016.N().S(`:

`)
	//line bf.qtpl:90
	qw422016.E().S(commentText)
	//line bf.qtpl:90
	qw422016.N().S(`
*/
func (bf *`)
	//line bf.qtpl:92
	qw422016.E().S(bfName)
	//line bf.qtpl:92
	qw422016.N().S(`) `)
	//line bf.qtpl:92
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:92
	qw422016.E().S(flagName)
	//line bf.qtpl:92
	qw422016.N().S(`() bool {
	return bf.Get(`)
	//line bf.qtpl:93
	qw422016.E().S(constPrefix)
	//line bf.qtpl:93
	qw422016.E().S(flagName)
	//line bf.qtpl:93
	qw422016.N().S(`)
}

/*
Set`)
	//line bf.qtpl:97
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:97
	qw422016.E().S(flagName)
	//line bf.qtpl:97
	qw422016.N().S(` sets the bit at offset `)
	//line bf.qtpl:97
	qw422016.E().S(offset)
	//line bf.qtpl:97
	qw422016.N().S(` to the specfied value:

`)
	//line bf.qtpl:99
	qw422016.E().S(commentText)
	//line bf.qtpl:99
	qw422016.N().S(`

Prefer Put`)
	//line bf.qtpl:101
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:101
	qw422016.E().S(flagName)
	//line bf.qtpl:101
	qw422016.N().S(`()/Clear`)
	//line bf.qtpl:101
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:101
	qw422016.E().S(flagName)
	//line bf.qtpl:101
	qw422016.N().S(`()
*/
func (bf *`)
	//line bf.qtpl:103
	qw422016.E().S(bfName)
	//line bf.qtpl:103
	qw422016.N().S(`) Set`)
	//line bf.qtpl:103
	qw422016.E().S(methodPrefix)
	//line bf.qtpl:103
	qw422016.E().S(flagName)
	//line bf.qtpl:103
	qw422016.N().S(`(val bool) {
	bf.Set(`)
	//line bf.qtpl:104
	qw422016.E().S(constPrefix)
	//line bf.qtpl:104
	qw422016.E().S(flagName)
	//line bf.qtpl:104
	qw422016.N().S(`, val)
}
`)
//line bf.qtpl:106
}

//line bf.qtpl:106
func WriteFlagOperations(qq422016 qtio422016.Writer, bfName, constPrefix, methodPrefix, flagName, commentText, offset string) {
	//line bf.qtpl:106
	qw422016 := qt422016.AcquireWriter(qq422016)
	//line bf.qtpl:106
	StreamFlagOperations(qw422016, bfName, constPrefix, methodPrefix, flagName, commentText, offset)
	//line bf.qtpl:106
	qt422016.ReleaseWriter(qw422016)
//line bf.qtpl:106
}

//line bf.qtpl:106
func FlagOperations(bfName, constPrefix, methodPrefix, flagName, commentText, offset string) string {
	//line bf.qtpl:106
	qb422016 := qt422016.AcquireByteBuffer()
	//line bf.qtpl:106
	WriteFlagOperations(qb422016, bfName, constPrefix, methodPrefix, flagName, commentText, offset)
	//line bf.qtpl:106
	qs422016 := string(qb422016.B)
	//line bf.qtpl:106
	qt422016.ReleaseByteBuffer(qb422016)
	//line bf.qtpl:106
	return qs422016
//line bf.qtpl:106
}
