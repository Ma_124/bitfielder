package examples

//go:generate go build -o bf.exe ..
//go:generate ./bf.exe -dir . -type Bf -out bitfields_gen.go -aux -marshallers json,yaml

// Bf-Doc
// Flags:
// 	0 Zero
// 	1 One
// 	2 Two
// 	3 Three With Documentation
// 	4 Four too
// 	5 Five
// 	6 LongName
type Bf uint8
