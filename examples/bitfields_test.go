package examples

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v2"
	"testing"
)

func TestNewBf(t *testing.T) {
	allTrue := (1 << 8) - 1
	assert.Equal(t, "11111111", fmt.Sprintf("%b", allTrue))

	bf := NewBf()
	assert.Equalf(t, Bf(0), bf, "NewBf()")
	bf.ToggleAll()
	assert.Equalf(t, Bf(allTrue), bf, "ToggleAll()")
	bf.ClearAll()
	assert.Equalf(t, Bf(0), bf, "ClearAll()")
	bf.PutAll()
	assert.Equalf(t, Bf(allTrue), bf, "PutAll()")
	bf.ToggleAll()
	assert.Equalf(t, Bf(0), bf, "ToggleAll()")

	bf.SetOne(true)
	assert.Equalf(t, true, bf.Get(One), "SetOne(true)")

	bf.Set(Zero|Five, true)
	assert.Equalf(t, true, bf.Zero(), "Set(Zero | Five)")
	assert.Equalf(t, true, bf.Five(), "Set(Zero | Five)")

	bf.Toggle(LongName)
	assert.Equalf(t, true, bf.Get(LongName), "Toggle(LongName)")

	bf.Set(LongName, false)
	assert.Equalf(t, false, bf.LongName(), "Set(LongName, false)")
}

const bfExample = Zero | Two | Four | LongName

var bfAuxExample = &BfAux{
	Zero:     true,
	Two:      true,
	Four:     true,
	LongName: true,
}

const bfJson = `{"zero":true,"one":false,"two":true,"three":false,"four":true,"five":false,"long_name":true}`
const bfYaml = `zero: true
one: false
two: true
three: false
four: true
five: false
long-name: true
`

func TestBf_ToAux(t *testing.T) {
	bf := NewBf()
	bf.Put(bfExample)

	assert.Equal(t, bfAuxExample, bf.ToAux())
}

func TestBfAux_ToBf(t *testing.T) {
	assert.Equal(t, bfExample, bfAuxExample.ToBf())
}

func TestBf_MarshalJSON(t *testing.T) {
	data, err := json.Marshal(bfExample)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, bfJson, string(data))
}

func TestBf_MarshalJSONPtr(t *testing.T) {
	bf := bfExample
	data, err := json.Marshal(&bf)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, bfJson, string(data))
}

func TestBf_UnmarshalJSON(t *testing.T) {
	bf := NewBf()

	err := json.Unmarshal([]byte(bfJson), &bf)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, bfExample, bf)
}

func TestBf_UnmarshalYAML(t *testing.T) {
	bf := NewBf()

	err := yaml.Unmarshal([]byte(bfYaml), &bf)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, bfExample, bf)
}

func TestBf_MarshalYAML(t *testing.T) {
	data, err := yaml.Marshal(bfExample)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, bfYaml, string(data))
}
